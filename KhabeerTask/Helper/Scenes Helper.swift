//
//  Scenes Helper.swift
//  KhabeerTask
//
//  Created by mac on 01/12/2021.
//

import UIKit


enum SceneCreate{
    case controller(_ story: StoryBoards,_ vc: Controller)
    
    var call: UIViewController{
        switch self {
        case .controller(let story, let vc):
            return UIStoryboard(name: story.rawValue, bundle: Bundle.main).instantiateViewController(identifier: vc.rawValue)
        }
        
    }
}

enum StoryBoards: String{
    //BASE
    case MainST = "Main"
    
    //Home
    case HomeST = "Home"
}


enum Controller: String{
    case HomeVC = "HomeVC"
}


enum XIBs: String{
    //Home
    case HomeOfferCell = "OfferCell"
    case HomeCategoryCell = "CategoryCell"
}
