//
//  Views.swift
//  KhabeerTask
//
//  Created by mac on 30/11/2021.
//

import UIKit

var ChatButton: UIButton = {
    var button = UIButton(frame: CGRect(x: 0, y: 0, width: 40, height: 40))
    button.setImage(UIImage(systemName: "message"), for: .normal)
    return button
}()

var backButton: UIButton = {
    var button = UIButton(frame: CGRect(x: 0, y: 0, width: 40, height: 40))
    let arrowDirection = app_lang == "ar" ? "right" : "left"
    button.setImage(UIImage(systemName: "arrow.\(arrowDirection)"), for: .normal)
    return button
}()
