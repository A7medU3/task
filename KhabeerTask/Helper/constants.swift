//
//  constants.swift
//  KhabeerTask
//
//  Created by mac on 30/11/2021.
//

import Foundation
import UIKit
///store the current language that used in app when it in active mode
var app_lang = "en"

///the identifire of the notification center that end editing tap on screen
var endEditingNotificationIdentifire = "endEditingNotificationIdentifire"

///net flag that store the status of the internet
var net: Bool = true

/// shadow color of every View
var shadowColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.2523056193)

/// place holder image  for images not loaded yet
//var placeHolderImage = #imageLiteral(resourceName: "userDefault")


