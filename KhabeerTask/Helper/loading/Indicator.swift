//
//  Indicator.swift
//  test app 2
//
//  Created by Ahmed on 2/10/21.
//

import Foundation
import UIKit
import Lottie


/// loading class controller
@available(iOS 13.0, *)
class Indicator {
    
    /// static inistance
    static let shared = Indicator()
    
    /// the container of the loading file lottie
    private var containerView: UIView = {
        let view = UIView()
        view.frame = UIWindow(frame: UIScreen.main.bounds).frame
        view.center = UIWindow(frame: UIScreen.main.bounds).center
        view.backgroundColor = #colorLiteral(red: 0.2039215686, green: 0.5960784314, blue: 0.8588235294, alpha: 0.5)
        return view
    }()
    
    /// loading view
    private var progressView: AnimationView = {
        let view = AnimationView(name: "loading")
        view.frame = CGRect(x: 0, y: 0, width: 200, height: 200)
        view.center = UIWindow(frame: UIScreen.main.bounds).center
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .clear
        view.clipsToBounds = true
        view.layer.cornerRadius = 10
        view.loopMode = .loop
        view.animationSpeed = 1
        return view
    }()
    
    /// show loading and set it in keywindow view
    func showProgressView() {
        containerView.addSubview(progressView)
        progressView.play()
        if #available(iOS 13.0, *) {
            let keyWindow = UIApplication.shared.connectedScenes
                    .filter({$0.activationState == .foregroundActive})
                    .compactMap({$0 as? UIWindowScene})
                    .first?.windows
                    .filter({$0.isKeyWindow}).first
            keyWindow?.addSubview(containerView)
        }else{
            UIApplication.shared.keyWindow?.addSubview(containerView)
        }
    }
    
    /// hide loading
    func hideProgressView() {
        progressView.stop()
        containerView.removeFromSuperview()
    }
    
//    func createActivity() -> AnimationView{
//        progressView.play()
//        return progressView
//    }
    
    /// create loading view to use it in image loading
    /// - Returns: return loading indicator
    func createActivityIndicator() -> UIActivityIndicatorView{
        let activityView = UIActivityIndicatorView(style: .large)
        activityView.color = #colorLiteral(red: 0.09263247997, green: 0.582418859, blue: 0.741225183, alpha: 1)
        activityView.startAnimating()
        return activityView
    }
}
