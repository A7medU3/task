//
//  labelEX.swift
//  AlShaden
//
//  Created by apple on 7/5/20.
//  Copyright © 2020 Atiaf. All rights reserved.
//

import Foundation
import UIKit

/// the text fade type
enum fade{
    case In, Out
}

extension UILabel {
    
    /// convert html String to styled text
    /// - Parameter text: the htmlText
  func setHTMLFromString(text: String) {
    let modifiedFont = NSString(format:"<span style=\"font-family: \(self.font!.fontName); font-size: \(14)\">%@</span>" as NSString, text)
    let attrStr = try! NSAttributedString(
      data: modifiedFont.data(using: String.Encoding.unicode.rawValue, allowLossyConversion: true)!,
      options: [NSAttributedString.DocumentReadingOptionKey.documentType:NSAttributedString.DocumentType.html, NSAttributedString.DocumentReadingOptionKey.characterEncoding: String.Encoding.utf8.rawValue],
      documentAttributes: nil)
    self.attributedText = attrStr
  }
    
    
//    @IBInspectable var fontSizeLabel: CGFloat{
//        set {
//            font = UIFont(descriptor: UIFontDescriptor(name: "Cairo-Bold", size: newValue), size: newValue)
//        }
//        get{
//            return layer.shadowRadius
//        }
//    }
    
    /// set direction of text in label
    func setDirection(){
        if app_lang == "ar"{
            self.textAlignment = .right
        }else{
            self.textAlignment = .left
        }
    }
    
    /// force text to be in custom direction
    /// - Parameter direction: the custom direction 
    func forceDirection(direction: NSTextAlignment){
        self.textAlignment = direction
    }
    
    /// fade the text with alpha to show it as disappear and appear
    /// - Parameter type: the fade direction 
    func fade(type: fade){
        switch type{
        
        case .In:
            UIView.animate(withDuration: 1, delay: 0, options: .curveEaseIn) {
                self.alpha = 1
            }

        case .Out:
            UIView.animate(withDuration: 1, delay: 0, options: .curveEaseOut) {
                self.alpha = 0
            }
        }
        
    }
    
}
