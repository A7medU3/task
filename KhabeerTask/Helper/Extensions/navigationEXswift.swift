//
//  navigationEX.swift
//  Nazan
//
//  Created by apple on 6/21/20.
//  Copyright © 2020 Atiaf. All rights reserved.
//

import Foundation
import UIKit

extension UIColor {
    
    /// Converts this `UIColor` instance to a 1x1 `UIImage` instance and returns it.
    ///
    /// - Returns: `self` as a 1x1 `UIImage`.
    func as1ptImage() -> UIImage {
        UIGraphicsBeginImageContext(CGSize(width: 1, height: 1))
        setFill()
        UIGraphicsGetCurrentContext()?.fill(CGRect(x: 0, y: 0, width: 1, height: 1))
        let image = UIGraphicsGetImageFromCurrentImageContext() ?? UIImage()
        UIGraphicsEndImageContext()
        return image
    }
}

extension UINavigationController {
    
    //    func setNavigationBarBorderColor(_ color:UIColor) {
    //        self.navigationBar.shadowImage = color.as1ptImage()
    //    }
    
    
    /// set style for navigation bar such as
    /// - shadow color and opacity
    /// - Translucent style
    ///
    /// - Parameters:
    ///   - hidden: detect the navigation bar is hidden or not
    ///   - shadow: detect the navigation bar is have shadow or not
    func set_style(hidden: Bool){
        self.navigationBar.isTranslucent = hidden
        self.setNavigationBarHidden(hidden, animated: true)
    }
    
    
    /// create navigation with ckear Style
    func createClearNaviagtion(){
        self.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationBar.shadowImage = UIColor.clear.as1ptImage()
        self.navigationBar.isTranslucent = true
        self.navigationBar.backgroundColor = .clear
        let textAttributes = [NSAttributedString.Key.foregroundColor:UIColor.blue,
                              NSAttributedString.Key.font: UIFont(name: "Cairo-Regular", size: 20) ]
        self.navigationBar.titleTextAttributes = textAttributes
    }
    
    /// create navigation with bg Style
    func createClearNaviagtion(bgColor: UIColor){
        self.navigationBar.barTintColor = UIColor.white
        self.navigationBar.setBackgroundImage(bgColor.as1ptImage(), for: .default)
        self.navigationBar.shadowImage = bgColor.as1ptImage()
        self.navigationBar.isTranslucent = true
        let textAttributes = [NSAttributedString.Key.foregroundColor:UIColor.white,
                              NSAttributedString.Key.font: UIFont.systemFont(ofSize: 20, weight: .heavy) ]
        self.navigationBar.titleTextAttributes = textAttributes
    }
    
    
}



//MARK: - navigation bar
extension UINavigationBar{
    enum ViewSide {
        case Left, Right, Top, Bottom
    }
    
    func addBorder(toSide side: ViewSide, withColor color: CGColor, andThickness thickness: CGFloat) {
        
        let border = CALayer()
        border.backgroundColor = color
        
        switch side {
        case .Left: border.frame = CGRect(x: frame.minX, y: frame.minY, width: thickness, height: frame.height); break
        case .Right: border.frame = CGRect(x: frame.maxX, y: frame.minY, width: thickness, height: frame.height); break
        case .Top: border.frame = CGRect(x: frame.minX, y: frame.minY, width: frame.width, height: thickness); break
        case .Bottom: border.frame = CGRect(x: frame.minX, y: frame.maxY, width: frame.width, height: thickness); break
        }
        
        layer.addSublayer(border)
    }
}
