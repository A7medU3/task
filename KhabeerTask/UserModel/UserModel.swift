//
//  UserModel.swift
//  test app 2
//
//  Created by Ahmed on 2/10/21.
//

import Foundation
import MOLH


class UserModelKeys{
    var lang = "language"
    var login = "login"
    
}

class UserModel{
    private var Keys = UserModelKeys()
    
    static var shared: UserModel = {
        let object = UserModel()
        return object
    }()
    
    private init(){}
    
    //MARK: - setters
    func setLang(lang: String){
        UserDefaults.standard.setValue(lang, forKey: Keys.lang)
        MOLH.setLanguageTo(lang)
        app_lang = lang
    }
    
    
    func setLogin(value: Bool){
        UserDefaults.standard.setValue(value, forKey: Keys.login)
    }
    
    
    //MARK: - getters
    func getLanguage() -> String { UserDefaults.standard.value(forKey: Keys.lang) as? String ?? "" }
    func getLogin() -> Bool{ return UserDefaults.standard.value(forKey: Keys.login) as? Bool ?? false }
    
    
    
    //MARK: - logOut
    func logOut(){
        
    }
    
}
