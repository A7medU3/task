//
//  HomeVC + collection delegate.swift
//  KhabeerTask
//
//  Created by mac on 30/11/2021.
//

import UIKit

extension HomeVC: UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UICollectionViewDataSource{
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == offersCollection{
            return CGSize(width: offerItemWidth, height: offerItemHeight)
        }else{
            return CGSize(width: categoryItemWidth, height: categoryItemHeight)
        }
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == offersCollection{
            return presenter?.numberOfOffers ?? 0
        }else{
            return presenter?.numberOfCategories ?? 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == offersCollection{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "OfferCell", for: indexPath) as! OfferCell
            presenter?.configure(cell: cell, at: indexPath.item)
            return cell
        }else{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CategoryCell", for: indexPath) as! CategoryCell
            presenter?.configure(cell: cell, at: indexPath.item)
            return cell
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let visibleRect = CGRect(origin: offersCollection.contentOffset, size: offersCollection.bounds.size)
        let visiblePoint = CGPoint(x: visibleRect.midX, y: visibleRect.midY)
        let visibleIndexPath = offersCollection.indexPathForItem(at: visiblePoint)
        timerCounter = visibleIndexPath?.item ?? 0
   }
    
}
