//
//  OfferCell.swift
//  KhabeerTask
//
//  Created by mac on 30/11/2021.
//

import UIKit

protocol OfferCellViewDelegate{
    func setImage(url: String)
    func setCat(name: String, colorHex: String)
}

class OfferCell: UICollectionViewCell, OfferCellViewDelegate {
    
    //MARK: - outlets
    @IBOutlet weak var offerImgView: UIView!
    @IBOutlet weak var brushImg: UIImageView!
    @IBOutlet weak var offerImage: UIImageView!
    @IBOutlet weak var categoryName: UILabel!
    
    //MARK: - awake
    override func awakeFromNib() {
        super.awakeFromNib()
        offerImgView.shadow(color: shadowColor)
        if app_lang == "ar"{
            brushImg.transform = CGAffineTransform(scaleX: -1, y: 1)
            offerImage.transform = CGAffineTransform(scaleX: -1, y: 1)
        }
    }
    
    //MARK: - functions
    func setImage(url: String) {
        offerImage.setIMG(img: url)
    }
    
    func setCat(name: String, colorHex: String) {
        categoryName.text = name
        categoryName.textColor = colorHex.ToUIColor
    }
}
