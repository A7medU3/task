//
//  CategoryCell.swift
//  KhabeerTask
//
//  Created by mac on 01/12/2021.
//

import UIKit

protocol CategoryCellViewDelegate{
    func setCat(img: String, name: String)
}

class CategoryCell: UICollectionViewCell, CategoryCellViewDelegate {

    //MARK: - outlets
    @IBOutlet weak var imageView: UIView!
    @IBOutlet weak var catImg: UIImageView!
    @IBOutlet weak var catName: UILabel!
    
    //MARK: - didLoad
    override func awakeFromNib() {
        super.awakeFromNib()
        imageView.shadow(color: shadowColor)
    }

    //MARK: - functions
    func setCat(img: String, name: String) {
        catImg.setIMG(img: img)
        catName.text = name
    }
}
