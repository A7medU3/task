//
//  HomeRouter.swift
//  KhabeerTask
//
//  Created by mac on 30/11/2021.
//

import Foundation
import UIKit

class HomeRouter: HomeRouterProtocol{
    
    weak var VC: HomeViewProtocol?
    
    static func createModule() -> UIViewController{
        let view = SceneCreate.controller(.HomeST,.HomeVC).call as! HomeVC
        let interactor = HomeInteractor()
        let router = HomeRouter()
        let presenter = HomePresenter(view: view, interactor: interactor, router: router, error: view)
        let offerWorker = OfferWorker()
        let categoryWorker = CategoryWorker()
        view.presenter = presenter
        interactor.presenter = presenter
        interactor.error = presenter
        interactor.offerWorker = offerWorker
        interactor.catWorker = categoryWorker
        router.VC = view
        return view
    }
    
}
