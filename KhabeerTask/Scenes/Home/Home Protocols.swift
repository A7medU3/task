//
//  Home Protocols.swift
//  KhabeerTask
//
//  Created by mac on 30/11/2021.
//

import Foundation


protocol HomeViewProtocol: AnyObject{
    var presenter: HomePresenterProtocol? { get set }
    func reloadOffers()
    func noOffers()
    func startTimer()
    func reloadCategories()
    func noCategories()
}

protocol HomePresenterProtocol: AnyObject{
    var view: HomeViewProtocol? { set get }
    var numberOfOffers: Int? { get }
    var numberOfCategories: Int? { get }
    func viewDidload()
    func configure(cell: OfferCellViewDelegate, at index: Int)
    func configure(cell: CategoryCellViewDelegate, at index: Int)
    
}

protocol HomeInteractorInputProtocol{
    func getOffers()
    func getCategories()
}

protocol HomeIntreractorOutputProtocol: AnyObject{
    func offersFeatchedSuccessfully(model: PromoModel)
    func categoriesFeatchedSuccessfully(model: CategoryModel)
}

protocol HomeRouterProtocol{
    
}
