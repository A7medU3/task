//
//  HomeInteraactor.swift
//  KhabeerTask
//
//  Created by mac on 30/11/2021.
//

import Foundation

class HomeInteractor: HomeInteractorInputProtocol{
    
    weak var presenter: HomeIntreractorOutputProtocol?
    var error: errorProtocol?
    var offerWorker: OfferWorkerProtocol?
    var catWorker: CategoryWorkerProtocol?
    
    func getOffers() {
        offerWorker?.getNew(compilition: { [weak self] result, statusCode in
            guard let self = self else { return }
            switch result{
            case .success(let model):
                if statusCode == 200{
                    self.presenter?.offersFeatchedSuccessfully(model: model)
                }else{
                    self.error?.errorAlert(messasge: model.MessageDetail ?? "")
                }
                
                break
            case .failure(let error):
                self.error?.errorAlert(messasge: error.localizedDescription)
                break
            }
        })
    }
    
    func getCategories() {
        catWorker?.getAll(compilition: { [weak self] result, statusCode in
            guard let self = self else { return }
            switch result{
            case .success(let model):
                if statusCode == 200{
                    self.presenter?.categoriesFeatchedSuccessfully(model: model)
                }else{
                    self.error?.errorAlert(messasge: model.MessageDetail ?? "")
                }
                
                break
            case .failure(let error):
                self.error?.errorAlert(messasge: error.localizedDescription)
                break
            }
        })
    }
}
