//
//  Home Presenter.swift
//  KhabeerTask
//
//  Created by mac on 30/11/2021.
//

import Foundation

class HomePresenter: HomePresenterProtocol{
    
    //MARK: - variables
    weak var view: HomeViewProtocol?
    var interactor: HomeInteractorInputProtocol?
    var router: HomeRouterProtocol?
    var error: errorProtocol?
    var numberOfOffers: Int?
    var numberOfCategories: Int?
    var offers = [PromoDataModel]()
    var categories = [CategoryDataModel]()
    
    //MARK: - did laod
    init(view: HomeViewProtocol,
         interactor: HomeInteractorInputProtocol,
         router: HomeRouterProtocol,
         error: errorProtocol){
        
        self.view = view
        self.interactor = interactor
        self.router = router
        self.error = error
    }
    
    //MARK: - functions
    func viewDidload(){
        DispatchQueue.global(qos: .background).async {
            self.interactor?.getOffers()
        }
        
        DispatchQueue.global(qos: .background).async {
            self.interactor?.getCategories()
        }
    }
    
    func configure(cell: OfferCellViewDelegate, at index: Int) {
        let obj = offers[index]
        let url = URls()
        let imgURL = url.promoImgDomain + "\(obj.Id ?? 0)" + ".png"
        cell.setImage(url: imgURL)
        let name = (app_lang == "en") ? (obj.EnglishName ?? "") : (obj.ArabicName ?? "")
        cell.setCat(name: name, colorHex: obj.Color ?? "")
    }
    
    func configure(cell: CategoryCellViewDelegate, at index: Int) {
        let obj = categories[index]
        let url = URls()
        let name = (app_lang == "en") ? (obj.EnglishName ?? "") : (obj.ArabicName ?? "")
        let imgURL = url.catImgDomain + (obj.GroupCode ?? "0") + ".png"
        cell.setCat(img: imgURL, name: name)
    }
}

//MARK: - outPut interactor
extension HomePresenter: HomeIntreractorOutputProtocol{
    func categoriesFeatchedSuccessfully(model: CategoryModel) {
        numberOfCategories = model.Data?.count
        categories = model.Data ?? []
        DispatchQueue.main.async {
            if model.Data?.isEmpty ?? true{
                self.view?.noCategories()
            }else{
                self.view?.reloadCategories()
            }
        }
    }
    
    
    func offersFeatchedSuccessfully(model: PromoModel) {
        numberOfOffers = model.Data?.count
        offers = model.Data ?? []
        DispatchQueue.main.async {
            if model.Data?.isEmpty ?? true{
                self.view?.noOffers()
            }else{
                self.view?.reloadOffers()
                self.view?.startTimer()
            }
        }
    }
}

//MARK: - error
extension HomePresenter: errorProtocol{
    func errorAlert(messasge: String) {
        error?.errorAlert(messasge: messasge)
    }
}
