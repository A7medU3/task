//
//  HomeVC.swift
//  KhabeerTask
//
//  Created by mac on 30/11/2021.
//

import UIKit

class HomeVC: UIViewController {

    //MARK: - variables
    var presenter: HomePresenterProtocol?
    var timer: Timer!
    var timerCounter = 0
    var offerItemHeight: CGFloat = 180
    var offerItemWidth: CGFloat = UIScreen.main.bounds.width-32
    let numberOfCatItemsPerRow = 4
    var categoryItemHeight: CGFloat = 0
    var categoryItemWidth: CGFloat = 0
    
    //MARK: - outlets
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var searchView: UIView!
    @IBOutlet weak var searchTF: UITextField!
    @IBOutlet weak var attachView: UIView!
    @IBOutlet weak var askView: UIView!
    @IBOutlet weak var offersStack: UIStackView!
    @IBOutlet weak var offersCollection: UICollectionView!
    @IBOutlet weak var categoriesStack: UIStackView!
    @IBOutlet weak var categoriesCollection: UICollectionView!
    @IBOutlet weak var categoriesCollectionHeight: NSLayoutConstraint!
    
    
    
    //MARK: - did load
    override func viewDidLoad() {
        super.viewDidLoad()
        Style(navigation: true)
        setUpUI()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        presenter?.viewDidload()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        timer.invalidate()
    }
    
    //MARK: - functions
    func setUpUI(){
        setXIBs()
        endTap()
        createNaviagtion()
        searchTF.setDirection()
        searchTF.setPlaceHolder(color: UIColor(named: ColorHelper.TFPlaceHolder.rawValue)!)
        searchView.shadow(color: shadowColor)
        attachView.shadow(color: shadowColor)
        askView.shadow(color: shadowColor)
        offersStack.isHidden = true
        categoriesStack.isHidden = true
        headerView.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMaxXMaxYCorner]
        calculateCategoryItemSize()
    }
    
    func createNaviagtion(){
        navigationController?.createClearNaviagtion()
        navigationItem.rightBarButtonItems = [UIBarButtonItem(customView: ChatButton)]
        navigationItem.leftBarButtonItems = [UIBarButtonItem(customView: backButton)]
    }
    
    func setXIBs(){
        offersCollection.register(UINib(nibName: XIBs.HomeOfferCell.rawValue, bundle: Bundle.main), forCellWithReuseIdentifier: XIBs.HomeOfferCell.rawValue)
        
        categoriesCollection.register(UINib(nibName: XIBs.HomeCategoryCell.rawValue, bundle: Bundle.main), forCellWithReuseIdentifier: XIBs.HomeCategoryCell.rawValue)
        
    }
    
    func calculateCategoryItemSize(){
        let emptySpaces = (numberOfCatItemsPerRow+1)*8
        categoryItemWidth = CGFloat(categoriesCollection.bounds.width-CGFloat(emptySpaces))/CGFloat(numberOfCatItemsPerRow)
        categoryItemHeight = categoryItemWidth+CGFloat(30)
    }
    
    func calculateCategoriesHeight(){
        let numberOfRows = CGFloat(presenter?.numberOfCategories ?? 0)/CGFloat(numberOfCatItemsPerRow)
        categoriesCollectionHeight.constant = (numberOfRows.rounded(.up))*categoryItemHeight
        
    }
    
    @objc func timerBlock(){
        if timerCounter == (presenter?.numberOfOffers ?? 0)-1{
            timerCounter = 0
        }else{
            timerCounter+=1
        }
        offersCollection.scrollToItem(at: IndexPath(item: timerCounter, section: 0), at: .centeredHorizontally, animated: true)
    }
    
    //MARK: - actions
    @objc func backAction(){
        
    }
}

//MARK: - view protocol
extension HomeVC: HomeViewProtocol{
    func reloadOffers() {
        offersStack.isHidden = false
        offersCollection.reloadData()
    }
    
    func noOffers(){
        offersStack.isHidden = true
    }
    
    func startTimer(){
        timer = Timer.scheduledTimer(timeInterval: 3, target: self, selector: #selector(HomeVC.timerBlock), userInfo: nil, repeats: true)
    }
    
    func reloadCategories() {
        categoriesStack.isHidden = false
        calculateCategoriesHeight()
        categoriesCollection.reloadData()
    }
    
    func noCategories() {
        categoriesStack.isHidden = true
    }
}


//MARK: - error protocol
extension HomeVC: errorProtocol{
    func errorAlert(messasge: String) {
        showAlert(withTitle: true, msg: messasge, compilition: nil)
    }
}
