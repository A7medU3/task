//
//  APIProtocol.swift
//  Q_managment
//
//  Created by Ahmed on 6/28/21.
//

import Foundation
import Alamofire

/// protocol contained perform requests functions
protocol APIClientProtocol: class{
    /// perform multiPart / formData request
    /// - Parameters:
    ///   - url: API URL
    ///   - method: HTTP Method
    ///   - parameters: HTTP body
    ///   - headersType: HTTP headers
    ///   - imgKey: array of the API images Keys
    ///   - img: array of images Data
    ///   - filesNames: array of images names
    ///   - mimeTypes: array of images mimeTypes
    ///   - completion: clusure to excute
    ///   - result: the result of decoded Model and error
    ///   - statusCode: the API statusCode
    /// - Returns: Alamofire Request
    @discardableResult
    func performRequest<T:Decodable>(url: String, method: HTTPMethod, parameters: Parameters?, headersType: httpHeadersType, fileUrlKey: [String], files:[Data], filesNames:[String], mimeTypes:[String], completion: @escaping (_ result:Result<T, AFError>,_ statusCode:Int?)->Void) -> DataRequest
    
    /// perform reguler request without parameters
    /// - Parameters:
    ///   - url: API URL
    ///   - method: HTTP Method
    ///   - headersType: HTTP headers
    ///   - indicatorType: custom control of loading style
    ///   - completion: clusure to excute
    ///   - result : the result of decoded Model and error
    ///   - statusCode: the API statusCode
    /// - Returns: Alamofire Request
    @discardableResult
    func performRequest<T:Decodable>(url: String, method: HTTPMethod, headersType: httpHeadersType, completion:@escaping (_ result: Result<T, AFError>,_ statusCode:Int?)->Void) -> DataRequest
    
    /// perform reguler request with parameters
    /// - Parameters:
    ///   - url: API URL
    ///   - method: HTTP Method
    ///   - RequestModel: generic body object
    ///   - parameterType: the type of the body of the request array of objects or object
    ///   - headersType: HTTP headers
    ///   - indicatorType: custom control of loading style
    ///   - completion: clusure to excute
    ///   - result : the result of decoded Model and error
    ///   - statusCode: the API statusCode
    /// - Returns: Alamofire Request
    @discardableResult
    func performRequest<T:Decodable, M:Encodable>(url: String, method: HTTPMethod, RequestModel: M?, headersType: httpHeadersType, completion:@escaping (_ result: Result<T, AFError>,_ statusCode:Int?)->Void) -> DataRequest
    
    /// download file from API
    /// - Parameters:
    ///   - url: download URL
    ///   - name: the name of the file
    ///   - compilition: clusure to excute
    ///   - status: the status of the code if it success or faild
    ///   - fileURL: the response fileURL
    ///   - error: error message
    func downloadFile(from url: String, with name: String, compilition: @escaping (_ status: Bool,_ fileURL: URL?,_ error: String?) -> Void)
    
    
    
    @discardableResult
    func performRequest2<T:Decodable, M:Encodable>(url: String, method: HTTPMethod, RequestModel: M?, headersType: HTTPHeaders, completion:@escaping (_ result: Result<T, AFError>,_ statusCode:Int?)->Void) -> DataRequest
    
}
