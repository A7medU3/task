//
//  Offer Worker.swift
//  KhabeerTask
//
//  Created by mac on 30/11/2021.
//

import Foundation
import Alamofire

class OfferWorker: OfferWorkerProtocol{
    func getNew(compilition: @escaping (Result<PromoModel, AFError>, Int?) -> Void) {
        Requests.API(indicator: .custom).performRequest(url: Requests.url.promos, method: .get, headersType: .none, completion: compilition)
    }
}
