//
//  Offer Worker Protocol.swift
//  KhabeerTask
//
//  Created by mac on 30/11/2021.
//

import Foundation
import Alamofire

protocol OfferWorkerProtocol{
    
    /// get offers list to show it in Home page
    func getNew(compilition: @escaping (_ result: Result<PromoModel, AFError>,_ statusCode: Int?) -> Void)
}
