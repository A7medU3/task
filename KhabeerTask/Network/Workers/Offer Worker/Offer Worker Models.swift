//
//  Offer Worker Models.swift
//  KhabeerTask
//
//  Created by mac on 30/11/2021.
//

import Foundation

class PromoModel: Codable{
    var Address:String?
    var Promo:String?
    var Success:Bool?
    var Code:Int?
    var EnglishMessage:String?
    var ArabicMessage:String?
    var CurrentPage:Int?
    var IsArabic:Bool?
    var PageCount:Int?
    var VisitStatus:Bool?
    var MessageDetail: String?
    var Data: [PromoDataModel]?
}

class PromoDataModel: Codable{
    var EnglishName: String?
    var ArabicName: String?
    var Color: String?
    var sort: Int?
    var IsSpecial: Bool?
    var Id: Int?
    var CreationDate: String?
    var LastUpdateDate: String?
    var IsDeleted: Bool?
}
