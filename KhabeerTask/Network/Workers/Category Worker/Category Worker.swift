//
//  Category Worker.swift
//  KhabeerTask
//
//  Created by mac on 01/12/2021.
//

import Foundation
import Alamofire

class CategoryWorker: CategoryWorkerProtocol{
    
    func getAll(compilition: @escaping (Result<CategoryModel, AFError>, Int?) -> Void) {
        Requests.API(indicator: .custom).performRequest(url: Requests.url.categories, method: .get, headersType: .none, completion: compilition)
    }
}
