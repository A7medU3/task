//
//  Category Protocols.swift
//  KhabeerTask
//
//  Created by mac on 01/12/2021.
//

import Foundation
import Alamofire

protocol CategoryWorkerProtocol{
    
    /// get all categories
    func getAll(compilition: @escaping (_ result: Result<CategoryModel, AFError>,_ statusCode: Int?) -> Void)
}
