//
//  Category Models.swift
//  KhabeerTask
//
//  Created by mac on 01/12/2021.
//

import Foundation

class CategoryModel: Codable{
    var Success: Bool?
    var Code: Int?
    var VisitStatus: Int?
    var MessageDetail: String?
    var Data: [CategoryDataModel]?
}

class CategoryDataModel: Codable{
    var ArabicName: String?
    var EnglishName: String?
    var ParentId: String?
    var GroupCode: String?
    var Sort: Int?
    var Id: Int?
    var CreationDate: String?
    var LastUpdateDate: String?
    var IsDeleted: Bool?
}
