//
//  API.swift
//  test app 2
//
//  Created by Ahmed on 2/10/21.
//

import Foundation
import Alamofire


/// indicator appearance type
enum IndicatorType {
    /// the regular loading of the application
    case regular
    /// custome one may be added in specific request
    case custom
}

/// types of header structure
enum httpHeadersType{
    /// add language header
    case langOnly
    /// add authurazation header
    case   token
    /// add language and authorization
    case   both
    /// add  no headers
    case   none
}

extension AFError{
    var noInterNet: String{
        return "check your internet Connection"
    }
}

class APIClient: APIClientProtocol {
    
    private var indicatorType : IndicatorType? = .regular
    
    /// initialize API Client Class with
    /// - Parameter indicatorType: indicator type
    init(indicatorType : IndicatorType? = .regular){
        self.indicatorType = indicatorType
    }
    
    /// get the headers depends on types
    /// - Parameter type: type of headers
    /// - Returns: http headers
    private func getHeaders(type:httpHeadersType) -> HTTPHeaders?{
        let HttpHeaders:HTTPHeaders = ["Accept":"*/*",
                                       "Content-Type": "application/json"]
        return HttpHeaders
    }
    
    
    
    //MARK: - perform MultiPart Request
    @discardableResult
    func performRequest<T:Decodable>(url: String, method: HTTPMethod, parameters: Parameters?, headersType: httpHeadersType, fileUrlKey: [String], files:[Data], filesNames:[String], mimeTypes:[String], completion: @escaping (_ result:Result<T, AFError>,_ statusCode:Int?)->Void) -> DataRequest {
        
        Indicator.shared.showProgressView()
        
        return AF.upload(multipartFormData: { (multipartFormData) in

            if let parameters = parameters {
                for (key, value) in parameters {
                    multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
                }
            }

            for index in 0..<fileUrlKey.count{
                multipartFormData.append(files[index], withName: fileUrlKey[index], fileName: filesNames[index], mimeType: "image/jpeg")
            }

        }, to: url, headers: getHeaders(type: headersType)).responseDecodable (decoder: JSONDecoder()){ (response: DataResponse<T, AFError>) in

            debugPrint(response)
            Indicator.shared.hideProgressView()
            completion(response.result, response.response?.statusCode)
        }
    }
    
    
   //MARK: - perform request without body
    @discardableResult
    func performRequest<T:Decodable>(url: String, method: HTTPMethod, headersType: httpHeadersType, completion:@escaping (_ result: Result<T, AFError>,_ statusCode:Int?)->Void) -> DataRequest {
        
        if self.indicatorType == .regular {
            Indicator.shared.showProgressView()
        }
        
        return AF.request(url, method: method, parameters: nil, encoding: JSONEncoding.default, headers: getHeaders(type: headersType)).responseDecodable (decoder: JSONDecoder()){ (response: DataResponse<T, AFError>) in

            if self.indicatorType == .regular {
                Indicator.shared.hideProgressView()
            }

            debugPrint(response)

            completion(response.result, response.response?.statusCode)
        }
    }
    
    //MARK: - peform request with body
    @discardableResult
    func performRequest<T:Decodable, M:Encodable>(url: String, method: HTTPMethod, RequestModel: M?, headersType: httpHeadersType, completion:@escaping (_ result: Result<T, AFError>,_ statusCode:Int?)->Void) -> DataRequest {
        
        if self.indicatorType == .regular {
            Indicator.shared.showProgressView()
        }
        
        return AF.request(url, method: method, parameters: RequestModel, encoder: JSONParameterEncoder.default, headers: getHeaders(type: headersType)).responseDecodable (decoder: JSONDecoder()){ (response: DataResponse<T, AFError>) in
            
            if self.indicatorType == .regular {
                Indicator.shared.hideProgressView()
            }
            
            debugPrint(response)
            
            completion(response.result, response.response?.statusCode)
        }
        
    }
    
    
    
    
    func performRequest2<T, M>(url: String, method: HTTPMethod, RequestModel: M?, headersType: HTTPHeaders, completion: @escaping (Result<T, AFError>, Int?) -> Void) -> DataRequest where T : Decodable, M : Encodable {
        
        if self.indicatorType == .regular {
            Indicator.shared.showProgressView()
        }
        
        return AF.request(url, method: method, parameters: RequestModel, encoder: JSONParameterEncoder.default, headers: headersType).responseDecodable (decoder: JSONDecoder()){ (response: DataResponse<T, AFError>) in
            
            if self.indicatorType == .regular {
                Indicator.shared.hideProgressView()
            }
            
            debugPrint(response)
            
            completion(response.result, response.response?.statusCode)
        }
    }
    
    //MARK: - download file
    func downloadFile(from url: String, with name: String, compilition: @escaping (_ status: Bool,_ fileURL: URL?,_ error: String?) -> Void){
        
        let dest: DownloadRequest.Destination = { _, _ in
            let documentsURL:NSURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first! as NSURL
            let fileURL = documentsURL.appendingPathComponent(name)
            return (fileURL!,[.removePreviousFile, .createIntermediateDirectories])
        }
        
        AF.download(url, to: dest).downloadProgress { (progress) in
            Indicator.shared.showProgressView()
        }.response { (response) in
            Indicator.shared.hideProgressView()
            if response.error == nil{
                compilition(true, response.fileURL, nil)
            }else{
                compilition(false, nil, response.error?.localizedDescription)
            }
        }
    }
    
}


