//
//  API Requests.swift
//  test app 2
//
//  Created by Ahmed on 2/10/21.
//

import Foundation
import Alamofire


/// request Class
class Requests{
    
    /// an ainistance from url constants
    static var url : URls{
        let urls = URls()
        return urls
    }
    
//    /// an inistance from network connctivity check
//    static let networkConnection = InterNet()
    
    
    /// API dependancy injection function for APIClient relation
    /// - Parameter indicator: indicatior type of the request
    /// - Returns: return protocol object to use its functions
    static func API(indicator: IndicatorType? = .regular) -> APIClientProtocol{
        let api : APIClientProtocol = APIClient(indicatorType: indicator)
        return api
    }
    
    
    private init(){}
}
