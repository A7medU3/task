//
//  API URls.swift
//  test app 2
//
//  Created by Ahmed on 2/10/21.
//

import Foundation

class URls{
    lazy fileprivate var Domain = "https://youmeda.com/api/Pharmacy"
    lazy fileprivate var testDomain = "http://40.127.194.127:5656/Pharmacy"
    
    lazy var promoImgDomain = "https://youmeda.com/Uploads/PharmactPromoCategories/"
    lazy var catImgDomain = "https://youmeda.com/Content/Design/img/DrugCategories/"
    
    lazy var promos = "\(testDomain)/Get_Pharmacy_Promotions_Category"
    lazy var categories = "\(Domain)/GetMedicationsGroups?GroupCode="
}
